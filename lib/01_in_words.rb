class Integer
  ONES = { 0 => "zero", 1 => "one", 2 => "two", 3 => "three", 4 => "four", 5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine" }
  TENS = { 10 => "ten", 20 => "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty", 90 => "ninety" }
  TEENS = { 11 => "eleven", 12 => "twelve", 13 => "thirteen", 14 => "fourteen", 15 => "fifteen", 16 => "sixteen", 17 => "seventeen", 18 => "eighteen", 19=> "nineteen" }
  MAGNITUDES = { 100 => "hundred", 1000 => "thousand", 1_000_000 => "million", 1_000_000_000 => "billion", 1_000_000_000_000 => "trillion" }

  def find_magnitude
    curr = []
    MAGNITUDES.select { |k, v| curr << k if k <= self }
    curr.last
  end

  def in_words
    number_in_words = ''
    number = self.to_s
    if self < 10
      number_in_words << ONES[number.to_i]
    elsif self > 10 && number.to_i < 20
      number_in_words << TEENS[number.to_i]
    elsif self < 100
      if self % 10 == 0
        number_in_words << TENS[number.to_i]
      else
        number_in_words << TENS[(number[0] + "0").to_i] + ' ' +  ONES[(number[-1]).to_i]
      end
    else
      magnitude = find_magnitude
      number_in_words << (self / magnitude).in_words + ' ' + MAGNITUDES[magnitude]
      if self % magnitude != 0
        number_in_words << ' ' + (self % magnitude).in_words
      else
        number_in_words
      end
    end
    number_in_words
  end
end
